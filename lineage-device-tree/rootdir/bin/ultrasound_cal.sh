#!/vendor/bin/sh

AUDIO_PATH="/data/vendor/audiohal"
CALIB_FILE="$AUDIO_PATH/upd_calib_v2"
PERSIST_AUDIO="/mnt/vendor/persist/factory/audio"
ULTRA_DEFAULT_FILE_NAME="/vendor/etc/motorola/12m/ultrasound.wav"
RUNNER=/vendor/bin/UPD_calib
SCRIPT_NAME="ultrasound_cal.sh"
ULTRASOUND_CAL_PROP="vendor.ultrasound.cal.action"
ULTRASOUND_PLAYBACK="vendor.ultrasound.playback_enable"
ULTRASOUND_PLAYBACK_FILENAME="vendor.ultrasound.playback_filename"
IS_FACTORY="ro.vendor.build.motfactory"
dd_start=0
dd_far=0
dd_near=0
dd_stop=0

debug()
{
    echo "Debug: $*"
}

notice()
{
    echo "Debug: $*"
    echo "$SCRIPT_NAME: $*" > /dev/kmsg
}

start_runner()
{
    if [ -f "$RUNNER" ]
    then
        sleep 2
        setprop $ULTRASOUND_CAL_PROP "running"
    else
        notice "$RUNNER dose not exists"
    fi
}

signal_runner()
{
    notice "send signal $1 to engine_runner"
    case "$1" in
        "stop")
            killall -INT UPD_calib
            ;;
        "far")
            killall -SIGUSR1 UPD_calib
            ;;
        "near")
            killall -SIGUSR2 UPD_calib
            ;;
    esac
}

copy_cal_data()
{
    prop=`getprop $IS_FACTORY`
    if [ "$prop" == "1" ]; then
        if [ -f "$CALIB_FILE" ]
        then
            cp "$CALIB_FILE" "$PERSIST_AUDIO/upd_calib_v2"
            if [ "$?" == "0" ]
            then
                notice "copy calibration data done!"
            fi

            if [ -f "$PERSIST_AUDIO/upd_calib_v2" ]
            then
                chown audio audio "$PERSIST_AUDIO/upd_calib_v2"
                chgrp system system "$PERSIST_AUDIO/upd_calib_v2"
                chmod 664 "$PERSIST_AUDIO/upd_calib_v2"
            else
                notice "$PERSIST_AUDIO/upd_calib_v2 dosen't exists!"
            fi
        fi
    fi
}


ultrasound_cal()
{
    while true
    do
        prop=`getprop $ULTRASOUND_CAL_PROP`
        notice "prop is $prop"
        if [ "$prop" == "start" ]; then
            sleep 1
            if [ $dd_start == 0 ]; then
                /system_ext/bin/AudioSetParam ultrasound-proximity=1
                sleep 0.5
                /system_ext/bin/AudioSetParam ultrasound-proximity=0
                sleep 0.5
                /system_ext/bin/AudioSetParam ultrasound-proximity=1
                /system_ext/bin/AudioSetParam ultrasound-proximity-cali=1
                dd_start=1
                dd_stop = 0
            fi
        elif [ "$prop" == "far" ]; then
            if [ $dd_far == 0 ]; then
                notice "--ultra far "
                /system_ext/bin/AudioSetParam ultrasound-proximity-cali=3
                dd_far=1
            fi
        elif [ "$prop" == "near" ]; then
            if [ $dd_near == 0 ]; then
                notice "--ultra near "
                /system_ext/bin/AudioSetParam ultrasound-proximity-cali=2
                dd_near=1
            fi
        elif [ "$prop" == "stop" ]; then
            if [ $dd_stop == 0 ]; then
                notice "--ultra stop "
                /system_ext/bin/AudioSetParam ultrasound-proximity-cali=0
                /system_ext/bin/AudioSetParam ultrasound-proximity=0
                dd_stop=1
                sleep 3
                dd_start=0
                dd_far=0
                dd_near=0
                copy_cal_data
            fi
            break;
        fi
        sleep 0.1
    done
}

while getopts "cp" op
do
    case "$op" in
        "c")
            notice "ultrasound calibration"
            ultrasound_cal
            ;;
        "p")
            notice "ultrasound playback"
            prop=`getprop $ULTRASOUND_PLAYBACK`
            notice "$ULTRASOUND_PLAYBACK is $prop"
            if [ "$prop" == "1" ]; then
                ultrasound_handset_playback
            fi
            ;;
        "h")
            usage
            ;;
        *)
            debug "unkown option arg"
            ;;
        esac
done

