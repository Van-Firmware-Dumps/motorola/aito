#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_aito.mk

COMMON_LUNCH_CHOICES := \
    lineage_aito-user \
    lineage_aito-userdebug \
    lineage_aito-eng
