#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from aito device
$(call inherit-product, device/motorola/aito/device.mk)

PRODUCT_DEVICE := aito
PRODUCT_NAME := lineage_aito
PRODUCT_BRAND := motorola
PRODUCT_MODEL := motorola razr 50
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="aito_g_sys-user 14 U3UCS34M.63-88-18-2 9cd8d5 release-keys"

BUILD_FINGERPRINT := motorola/aito_g_sys/aito:14/U3UCS34M.63-88-18-2/9cd8d5:user/release-keys
