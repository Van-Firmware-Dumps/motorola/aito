## user 14 U3UCS34.63-88-18-2 f760b release-keys
- Manufacturer: motorola
- Platform: common
- Codename: aito
- Brand: motorola
- Flavor: user
- Release Version: 14
- Kernel Version: 6.1.99
- Id: U3UCS34.63-88-18-2
- Incremental: f760b
- Tags: release-keys
- CPU Abilist: arm64-v8a
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: motorola/aito_g_hal/aito:14/U3UCS34.63-88-18-2/f760b:user/release-keys
- OTA version: 
- Branch: user-14-U3UCS34.63-88-18-2-f760b-release-keys-24137
- Repo: motorola/aito
